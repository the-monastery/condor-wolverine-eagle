import Assets.Bitmaps.Fire;
import Assets.Bitmaps.GreenDragon;
import Assets.Bitmaps.Knight;
import Assets.Bitmaps.OrangeDragon;
import Assets.Bitmaps.SpineyMonster;
import Assets.SimpleMovieClip;

import condor_wolverine_eagle.graphics.SimpleSprite;

import flash.display.Bitmap;
import flash.display.BitmapData;



public function OnWindowComplete() : void 
{
    addBitmap( new Knight() );
    addBitmap( new GreenDragon() );
    addBitmap( new OrangeDragon() );
    addBitmap( new Fire() );
    addBitmap( new SpineyMonster() );
    
}

public function addBitmap( data:BitmapData ) : void 
{
    var bitmap:Bitmap = new Bitmap( data );
    bitmap.y = Math.max( 0, Math.random() * stage.stageHeight - bitmap.height );
    bitmap.x = Math.max( 0, Math.random() * stage.stageWidth - bitmap.width );
    stage.addChild( bitmap );
}
package condor_wolverine_eagle.graphics
{
	import flash.display.Shape;
	import flash.display.Sprite;
	
	public class SimpleSprite extends Sprite
	{
		public function SimpleSprite()
		{
			var rectangle:Shape = new Shape();
			rectangle.graphics.beginFill( 0xFF0000 );
			rectangle.graphics.drawRect( 0, 0, 100,100 ); 
			rectangle.graphics.endFill(); 
			addChild(rectangle);
		}
	}
}